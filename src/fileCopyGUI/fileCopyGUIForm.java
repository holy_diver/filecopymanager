/**
 * Sample Skeleton for 'fileCopyGUIForm.fxml' Controller Class
 */
package fileCopyGUI;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Objects;

public class fileCopyGUIForm {
    int fileCount;
    @FXML // fx:id="srcText"
    private TextField srcText; // Value injected by FXMLLoader

    @FXML // fx:id="srcButton"
    private Button srcButton; // Value injected by FXMLLoader

    @FXML // fx:id="dstText"
    private TextField dstText; // Value injected by FXMLLoader

    @FXML // fx:id="dstButton"
    private Button dstButton; // Value injected by FXMLLoader

    @FXML // fx:id="filesListView"
    private ListView<String> filesListView; // Value injected by FXMLLoader

    @FXML // fx:id="startButton"
    private Button startButton; // Value injected by FXMLLoader

    @FXML // fx:id="fileProgressBar"
    private ProgressBar fileProgressBar; // Value injected by FXMLLoader

    @FXML // fx:id="totalProgressBar"
    private ProgressBar totalProgressBar; // Value injected by FXMLLoader

    DirectoryChooser directoryChooser;
    boolean processIsInProgress;

    @FXML
    void dstButtonOnAction(ActionEvent event) {
        directoryChooser = new DirectoryChooser();
        File directory;
        if ((directory = directoryChooser.showDialog(Stage.getWindows().get(0))) != null) {
            dstText.setText(directory.getAbsolutePath());
        }
    }

    @FXML
    void srcButtonOnAction(ActionEvent event) {
        directoryChooser = new DirectoryChooser();
        File directory;
        if ((directory = directoryChooser.showDialog(Stage.getWindows().get(0))) != null) {
            srcText.setText(directory.getAbsolutePath());
            filesListView.getItems().clear();
            addFilesInsideDirectoryToList(directory);
            fileCount = filesListView.getItems().size();
        }
    }

    void addFilesInsideDirectoryToList(File directory) {
        for (File f : Objects.requireNonNull(directory.listFiles())) {
            if (f.isDirectory()) {
                addFilesInsideDirectoryToList(f);
            } else {
                filesListView.getItems().add(f.getAbsolutePath());
            }
        }
    }

    @FXML
    void startButtonOnAction(ActionEvent event) throws IOException {
        if(processIsInProgress)
        {
            Alert processInProgress = new Alert(Alert.AlertType.ERROR);
            processInProgress.setHeaderText("Process Is In Progress!");
            processInProgress.setContentText("A copy process is already in progress.");
            processInProgress.showAndWait();
            return;
        }
        if (srcText.getText() != null && dstText.getText() != null) {
            Thread fileCopyThread = new Thread() {
                public void run() {
                    try {
                        processIsInProgress = true;
                        byte[] buffer = new byte[1024];
                        int readLength;
                        long totalWrittenSize = 0;
                        long currentFileSize = 0;
                        File destinationFile;
                        filesListView.setDisable(true);
                        for (int i = 0; i < fileCount; i++) {
                            String filePath = filesListView.getItems().get(i);
                            filesListView.getSelectionModel().select(i);
                            currentFileSize = Files.size(Paths.get(filePath));
                            FileInputStream fileToCopy = new FileInputStream(filePath);
                            destinationFile = new File(filePath.replace(srcText.getText(), dstText.getText()));
                            destinationFile.getParentFile().mkdirs();
                            FileOutputStream outputFile = new FileOutputStream(destinationFile.getAbsolutePath());
                            totalWrittenSize = 0;
                            while ((readLength = fileToCopy.read(buffer)) > 0) {
                                outputFile.write(buffer, 0, readLength);
                                totalWrittenSize += readLength;
                                fileProgressBar.setProgress(totalWrittenSize / (double)currentFileSize);
                            }
                            fileToCopy.close();
                            outputFile.close();
                            totalProgressBar.setProgress((i + 1) / (double)fileCount);
                        }
                        processIsInProgress = false;
                        filesListView.setDisable(false);
                    }
                    catch (Exception ex)
                    {

                    }
                }
            };
            fileCopyThread.start();
        }

    }
}
